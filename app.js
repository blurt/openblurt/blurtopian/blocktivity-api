require('dotenv').config()
const express = require('express');
const path = require('path');

const mongoose = require('mongoose');
const _ = require('lodash');

const api = require('./src/api');
const app = express();

var db1 = mongoose.createConnection(process.env.MONGODB_URI, { useNewUrlParser: true });
const {
  BlockchainStatsSchema,
  TxTypesSchema,
} = require('./src/schemas');
const BlockchainStats = db1.model('BlockchainStats', BlockchainStatsSchema);
const TxTypes = db1.model('TxTypes', TxTypesSchema);
const models = {
  BlockchainStats,
  TxTypes,
};

require("./lib/stream_v2.js").start(models);
require("./lib/stream_steem_v2.js").start(models);
require("./lib/stream_hive_v2.js").start(models);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', api({}, models));

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;