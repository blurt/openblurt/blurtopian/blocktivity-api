//This streamer uses a modified version of Rishi556's streamer with node switching, an implementation of which can be found at https://github.com/Rishi556/LEOVOTER/blob/main/index.js
//Said code is provided under MIT Licence

const axios = require("axios");
const moment = require("moment");
const _ = require("lodash");
const config = require("../config.json");
const constants = require("./constants");

const chain = "blurt";
let errorCount = 0;
let nodes = config[chain].nodes;
let currentNode = '';
switchNode();
let nodeErrorSwitch = config.nodeErrorSwitch;

function nodeError() {
  errorCount++;
  if (errorCount === nodeErrorSwitch) {
    switchNode();
  }
}

function switchNode() {
  errorCount = 0;
  currentNode = nodes.shift();
  nodes.push(currentNode);
}

async function processBlock(blockNumber, models) {
  axios.post(currentNode,
    {
      "id": blockNumber,
      "jsonrpc": "2.0",
      "method": "call",
      "params": ["database_api", "get_ops_in_block", [blockNumber, false]]
    }
  ).then((res) => {
    if (!_.isEmpty(res.data.result)) {
      let block = res.data.result;
      parseBlock(block, models);
      errorCount = 0;
    }
  }).catch(() => {
    nodeError();
  })

}

async function parseBlock(block, { BlockchainStats, TxTypes }) {
  if (block.length !== 0) {
    let ops_count = block.length;
    let trx_count = 0;

    let date_hour = moment().utc().format("YYYYMMDD-HH00");
    for (let i in block) {
      let trx = block[i];
      let { op } = trx;
      let tx_type = op[0];
      if (constants.transactions.includes(tx_type)) {
        trx_count = trx_count + 1;
      }

      let txTypes = await TxTypes.findOne({ chain, date_hour });
      if (txTypes) {
        let objStr = txTypes.json;
        let obj = JSON.parse(objStr);
        if(obj[tx_type]) {
          obj[tx_type] = obj[tx_type] + 1
        } else {
          obj[tx_type] = 1
        }
        txTypes.json = JSON.stringify(obj)
        await txTypes.save();

      } else {
        let obj = { }
        obj[tx_type] = 1
        var json = JSON.stringify(obj);
        let doc = new TxTypes({
          chain, date_hour, json,
          timestamp: new Date(),
        });
        await doc.save();
      }

    }
    try {
      let hour = moment().utc().format("HH00");
      let query = { chain, hour };
      let doc = await BlockchainStats.find(query);
      if (doc.length === 0) {
        let doc = new BlockchainStats(
          {
            ...query,
            operations: ops_count,
            transactions : trx_count,
            timestamp: new Date(),
          }
        );
        await doc.save();
      } else {
        await BlockchainStats.findOneAndUpdate(
          query,
          {
            $inc : {
              operations: ops_count,
              transactions : trx_count
            }
          }
        );
       }
    } catch (error) {
      console.log('error', error)
    }

  }
}

async function getLatestBlock() {
  return new Promise((resolve, reject) => {
    axios.post(currentNode,
      {
        "id": 0,
        "jsonrpc": "2.0",
        "method": "condenser_api.get_dynamic_global_properties",
        "params": []
      }
    ).then((res) => {
      if (res.data.result) {
        return resolve(res.data.result.last_irreversible_block_num);
      } else {
        switchNode();
        return reject();
      }
    }).catch(() => {
      switchNode();
      return reject();
    })
  })
}

async function start(models) {
  let end_block = 0;
  let current_block = 0;

  let startBlock = await getLatestBlock().catch(() => { return });
  current_block = startBlock;
  processBlock(current_block, models);
  current_block = current_block + 1;

  setInterval(async function () {
    end_block = await getLatestBlock().catch(() => { return });
  }, 2 * 1000);

  setInterval(function () {
    if (current_block < end_block) {
      processBlock(current_block, models)
      current_block = current_block + 1;
    }
  }, 2.5 * 1000);
}

module.exports = {
  start
}