//This streamer uses a modified version of Rishi556's streamer with node switching, an implementation of which can be found at https://github.com/Rishi556/LEOVOTER/blob/main/index.js
//Said code is provided under MIT Licence

const axios = require("axios");
const moment = require("moment");
const config = require("../config.json");
const constants = require("./constants");

let errorCount = 0;
let nodes = config.nodes;
switchNode();
let nodeErrorSwitch = config.nodeErrorSwitch;

async function getBlock(blockNumber, models) {
  let nextBlock = false;
  axios.post(currentNode, { "id": blockNumber, "jsonrpc": "2.0", "method": "call", "params": ["condenser_api", "get_block", [blockNumber]] }).then((res) => {
    if (res.data.result) {
      let block = res.data.result;
      nextBlock = true;
      parseBlock(block, models);
      errorCount = 0;
    }
  }).catch(() => {
  }).finally(() => {
    if (nextBlock) {
      setTimeout(() => {
        getBlock(blockNumber + 1, models);
      }, 0.5 * 1000);
    } else {
      nodeError();
      setTimeout(() => {
        getBlock(blockNumber, models);
      }, 3 * 1000);
    }
  })
}

function nodeError() {
  errorCount++;
  if (errorCount === nodeErrorSwitch) {
    switchNode();
  }
}

function switchNode() {
  errorCount = 0;
  currentNode = nodes.shift();
  nodes.push(currentNode);
}

async function parseBlock(block, { BlockchainStats }) {
  if (block.transactions.length !== 0) {
    let trxs = block.transactions;
    let ops_count = trxs.length;
    let trx_count = 0;
    for (let i in trxs) {
      let trx = trxs[i];
      const ops = trx.operations;
      for (let j in ops) {
        const op = ops[j];
        if (constants.transactions.includes(op[0])) {
          trx_count = trx_count + 1;
        }
      }
    }
    try {
      let hour = moment().utc().format("HH00");
      let doc = await BlockchainStats.findOneAndUpdate(
        { key: hour },
        {
          $inc : { operations: ops_count, transactions : trx_count }
        },
        {
          new: true,
          upsert: true // Make this update into an upsert
        }
      );
    } catch (error) {
      console.log('error', error)
    }

  }
}

async function getStartStreamBlock() {
  return new Promise((resolve, reject) => {
    axios.post(currentNode, { "id": 0, "jsonrpc": "2.0", "method": "condenser_api.get_dynamic_global_properties", "params": [] }).then((res) => {
      if (res.data.result) {
        return resolve(res.data.result.last_irreversible_block_num);
      } else {
        switchNode();
        startStreaming();
        return reject();
      }
    }).catch(() => {
      switchNode();
      startStreaming();
      return reject();
    })
  })
}

async function start(models) {
  let startBlock = await getStartStreamBlock().catch(() => { return });
  getBlock(startBlock, models);
}

module.exports = {
  start
}