const moment = require('moment');

const officerRoles = [
  'lgs', 'lrm', 'lsc','llc','ltr',
  'nof', 'ntr',
];

module.exports = {
  officerRoles,
};