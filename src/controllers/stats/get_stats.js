const moment = require("moment");
const get_stats = ({ BlockchainStats, TxTypes }, { config }) => async (req, res, next) => {
  const chain = req.query.chain || "blurt";
  const include_details = req.query.include_details || false;
  let now = moment().utc();
  try {
    let hour = now.subtract(10, "minutes").format("HH00");
    let result = await BlockchainStats.findOne({ chain, hour });
    if (result) {
      let data = {
        chain, hour,
        last_1h_op: result.operations,
        last_1h_tx: result.transactions
      };

      if (include_details) {
        const date_hour = now.subtract(10, "minutes").format("YYYYMMDD-HH00");
        let txTypesRes = await TxTypes.findOne({ chain, date_hour });
        data = {
          ...data,
          details: JSON.parse(txTypesRes.json)
        }
      }
      res.status(200).send({ ...data });
    } else {
      res.status(200).send({ });
    }

  } catch (error) {
    console.log('error', error)
    next(error);
  }
}

module.exports = { get_stats };