const moment = require("moment");
const get_stat_details = ({ TxTypes }, { config }) => async (req, res, next) => {
  const chain = req.query.chain || "blurt";
  const date_hour = req.query.date_hour || moment().utc().format("YYYYMMDD-HH00");
  try {
    let result = await TxTypes.findOne({ chain, date_hour });
    if (result) {
      res.status(200).send({
        chain, date_hour, details: JSON.parse(result.json)
      });
    } else {
      res.status(200).send({ });
    }

  } catch (error) {
    console.log('error', error)
    next(error);
  }
}

module.exports = { get_stat_details };