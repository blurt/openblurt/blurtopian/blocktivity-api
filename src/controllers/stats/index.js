const { Router: router } = require('express');

const { get_stats } = require('./get_stats');
const { get_stat_details } = require('./get_stat_details');

module.exports = (models, { config }) => {
  const api = router();

  api.get('/', get_stats(models, { config }));
  api.get('/details', get_stat_details(models, { config }));

  return api;
};
