const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const BlockchainStatsDailySchema = new Schema({
  chain: {
    type: String,
    required : true,
    dropDups: true
  },
  day: {
    type: String,
    required : true,
    dropDups: true
  },
  operations: {
    type: Number,
  },
  transactions: {
    type: Number,
  },
  timestamp: {
    type: Date,
    expires: '10080',
    default: Date.now
  },
});

BlockchainStatsDailySchema.index({ chain: 1, day: 1}, { unique: true })

module.exports = { BlockchainStatsDailySchema };