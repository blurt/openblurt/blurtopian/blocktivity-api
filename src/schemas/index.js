const { BlockchainStatsSchema } = require('./blockchain_stats');
const { TxTypesSchema } = require('./tx_types');
const { LastProcessedBlockSchema } = require('./last_processed_block');

module.exports = {
  BlockchainStatsSchema,
  LastProcessedBlockSchema,
  TxTypesSchema,
};