const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TxTypesSchema = new Schema({
  chain: {
    type: String,
    required : true,
    dropDups: true
  },
  date_hour: {
    type: String,
    required : true,
    dropDups: true
  },
  json: {
    type: String,
  },
  timestamp: {
    type: Date,
    expires: '8d',
    default: Date.now
  },
});

TxTypesSchema.index({ chain: 1, date_hour: 1}, { unique: true })

module.exports = { TxTypesSchema };