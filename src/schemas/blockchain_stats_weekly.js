const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const BlockchainStatsWeeklySchema = new Schema({
  chain: {
    type: String,
    required : true,
    dropDups: true
  },
  week: {
    type: String,
    required : true,
    dropDups: true
  },
  operations: {
    type: Number,
  },
  transactions: {
    type: Number,
  },
  timestamp: {
    type: Date,
    expires: '125m',
    default: Date.now
  },
});

BlockchainStatsWeeklySchema.index({ chain: 1, week: 1}, { unique: true })

module.exports = { BlockchainStatsWeeklySchema };