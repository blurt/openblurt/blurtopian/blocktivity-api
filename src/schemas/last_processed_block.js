const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const LastProcessedBlockSchema = new Schema({
  chain: {
    type: String,
    unique: true,
  },
  id: {
    type: String,
  },
  block_num: {
    type: Number,
  },
});

module.exports = { LastProcessedBlockSchema };