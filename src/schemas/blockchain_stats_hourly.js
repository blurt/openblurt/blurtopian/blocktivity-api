const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const BlockchainStatsHourlySchema = new Schema({
  chain: {
    type: String,
    required : true,
    dropDups: true
  },
  hour: {
    type: String,
    required : true,
    dropDups: true
  },
  operations: {
    type: Number,
  },
  transactions: {
    type: Number,
  },
  timestamp: {
    type: Date,
    expires: '125m',
    default: Date.now
  },
});

BlockchainStatsHourlySchema.index({ chain: 1, hour: 1}, { unique: true })

module.exports = { BlockchainStatsHourlySchema };