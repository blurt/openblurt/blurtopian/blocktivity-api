const express = require('express');

const { errorHandler } = require('../middleware');

// list of controllers here
const stats = require('../controllers/stats')

const routersInit = (config, models) => {
    const router = express();

    // register api points
    router.use('/stats', stats(models, { config }));

    // catch all api errors
    router.use(errorHandler);

    return router;
};

module.exports = routersInit;